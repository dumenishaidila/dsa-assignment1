import ballerina/graphql;

public type CovidEntry record {|
     string date;
    readonly string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recoveries?;
    decimal tested?;
|};

table<CovidEntry> key(region) covidEntriesTable = table [
    {date: "11/05/2022" ,region: "Erongo", confirmed_cases: 1303, deaths: 86, recoveries: 143, tested: 833},
    {date: "11/05/2022" ,region: "Khomas", confirmed_cases: 5936, deaths: 153, recoveries: 567, tested: 1456},
    {date: "11/05/2022" ,region: "Otjozondjupa", confirmed_cases:  698, deaths: 88, recoveries: 43, tested: 257}
];

public distinct service class CovidData {
    private final readonly & CovidEntry entryRecord;

    function init(CovidEntry entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }
    resource function get date() returns string {
        return self.entryRecord.date;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

 resource function get tested() returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested;
        }
        return;
    }

    resource function get deaths() returns decimal? {
        if self.entryRecord.deaths is decimal {
            return self.entryRecord.deaths;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries;
        }
        return;
    }

    resource function get active() returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested;
        }
        return;
    }
}

service /covid19 on new graphql:Listener(9000) {
    resource function get filter(string region) returns CovidData? {
        CovidEntry? covidEntry = covidEntriesTable[region];
        if covidEntry is CovidEntry {
            return new (covidEntry);
        }
        return;
    }

    remote function update(CovidEntry entry) returns CovidData? {
         CovidEntry? covidEntry = covidEntriesTable[entry.region];
          if covidEntry is CovidEntry {
            CovidEntry _ = covidEntriesTable.remove(entry.region);
            covidEntriesTable.add(entry);
            return new CovidData(entry);
        }
        return;
    }      
}