import ballerina/http;

Student [] students=[];

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

type Student record{|
    readonly decimal studentnumber;
    string name;
    string emailaddress;  
    string coursecode;
    decimal marks;
    |};
    
table<Student> key(studentnumber) student = table [
        {studentnumber: 221043220, name: "Killa Kau", emailaddress: "KillaKau22@gmail.com", coursecode:"WAD621S",marks :60},
        {studentnumber: 221047300, name: "Jason Kind", emailaddress: "JasonKind@gmail.com",coursecode:"WAD621S",marks :70},
        {studentnumber: 21911007, name: "Drake Onfroy", emailaddress: "credo@gmail.com",coursecode:"WAD621S",marks :67},
        {studentnumber: 221043500, name: "Tatiano Latoya", emailaddress: "latoya@gmail.com",coursecode:"WAD621S",marks :90}
    ];

service / on ep0 {
    resource function get students() returns Student[]{
        return students;
    }
    resource function post students (@http:Payload Student payload) returns  Student {
     students.push(payload);
     return payload;
    }

    resource function get students/[decimal studentnumber]() returns Student|http:NotFound {
        Student? students = student[studentnumber];
        if students is () {
            return http:NOT_FOUND;
        } else {
            return students;
        }
    }

    resource function delete students/[decimal studentnumber](@http:Payload Student students) returns Student|http:NotFound{
        Student? students1  = student[studentnumber];
        if students1 is (){
            return http:NOT_FOUND;
        }else{
            Student remove = student.remove(studentnumber);
            return remove;
        }
    }    
 }
